@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

    <div class="container">
        <h1>Liste des catégories</h1>
        <a href="{{route('categories.create')}}" class="btn btn-primary">Ajouter une nouvelle catégorie</a>

        <table id="tbl" class="table table-bordered table-striped mt-3">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Designation</th>
                    <th>Description</th>
                    <th colspan="3">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $cat)
                    <tr>
                        <td>{{$cat->id}}</td>
                        <td>{{$cat->designation}}</td>
                        <td>{{$cat->description}}</td>
                        <td><a href="{{route('categories.show', ["category" => $cat->id])}}" class="btn btn-info">Details</a></td>
                        <td><a href="{{route('categories.edit', ["category" => $cat->id])}}" class="btn btn-warning">Modifier</a></td>
                        <td>
                            <form action="{{route('categories.destroy', ["category" => $cat->id])}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Voulez-vous supprimer cette catégorie?')">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection
