@extends('layouts.admin')
@section('title', 'Gestion des clients and commandes')
@section('content')

    <div class="container">
        <h1>Validation de la Commande</h1>

        <form method="post" action="{{route('home.storeClientCommande')}}">
            @csrf
            <!-- Client information fields -->
            <div class="mb-3">
                <label for="nom" class="form-label">Nom:</label>
                <input type="text" class="form-control" name="nom" required>
            </div>

            <div class="mb-3">
                <label for="prenom" class="form-label">Prenom:</label>
                <input type="text" class="form-control" name="prenom" required>
            </div>

            <div class="mb-3">
                <label for="tele" class="form-label">Telephone:</label>
                <input type="text" class="form-control" name="tele" required>
            </div>

            <div class="mb-3">
                <label for="ville" class="form-label">Ville:</label>
                <input type="text" class="form-control" name="ville" required>
            </div>

            <div class="mb-3">
                <label for="adresse" class="form-label">Adresse:</label>
                <textarea class="form-control" name="adresse" rows="4" required></textarea>
            </div>

            <div class="mb-3">
                <label for="description" class="form-label">Description:</label>
                <textarea class="form-control" name="description" rows="8" cols="100" readonly>{{$strligne}}</textarea>
            </div>

            <div class="mb-3">
                <label for="prix_total" class="form-label">Prix total:</label>
                <input type="text" class="form-control" name="prix_total" value="{{$tot}}" readonly>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-primary">Commander</button>
        </form>
    </div>

@endsection
