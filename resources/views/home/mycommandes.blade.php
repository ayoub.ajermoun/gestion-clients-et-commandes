@extends('layouts.admin')
@section('title', 'Mon commandes')
@section('content')

    <h1>Hello {{$client_name}}</h1>
    <h3>ID: {{$client_id}}</h3>
    <h1>Mon Commandes:</h1>

    <table id="tbl" class="table table-bordered table-hover">
        <tr>
            <th>Id</th>
            <th>date_time</th>
            <th>prix_total</th>
            <th>description</th>
            <th>Actions</th>
        </tr>
        @foreach ($mycommandes as $id => $item)

            <tr>

                <td>{{$id}}</td>
                <td>{{$item['date_time']}}</td>
                <td>{{$item['prix_total']}} MAD</td>
                <td>
                    @php

                        $strligne = $item['description'];
                        $strligne = rtrim($strligne, "<br>");
                        $lines = explode("<br>", $strligne);
                        $products = [];

                        foreach ($lines as $line) {
                            $values = explode(",", $line);
                            $products[] = [
                                'id' => $values[0],
                                'designation' => $values[1],
                                'prix_u' => $values[2],
                                'qte' => $values[3],
                                'total' => $values[4],
                            ];
                        }
                    @endphp
                    @foreach ($products as $product)
                        <p>
                            <strong>Product ID: </strong>  {{ $product['id'] }} &nbsp; &nbsp; &nbsp;
                            <strong>Prix Unit: </strong>  {{ $product['prix_u'] }} MAD&nbsp;&nbsp;
                            <strong>Designation: </strong>  {{ $product['designation'] }}&nbsp;&nbsp;
                            <br>
                            <strong>Quantity:</strong>  {{ $product['qte'] }} &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;
                            <strong>Total: </strong>  {{ $product['total'] }} MAD
                        </p>
                        <hr>
                    @endforeach
                </td>
            </tr>
        @endforeach
        
    </table>

@endsection
