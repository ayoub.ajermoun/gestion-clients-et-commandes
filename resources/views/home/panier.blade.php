@extends('layouts.admin')
@section('title', 'Gestion des categories')
@section('content')

@if (empty($panier))
    <p>Votre panier est vide</p>
@else
    <div class="container">
        <h1>Mon panier</h1>

        <table id="tbl" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Designation</th>
                    <th>Prix unitaire</th>
                    <th>Quantite</th>
                    <th>Total ligne</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($panier as $id => $item)
                    <tr>
                        <td>{{$id}}</td>
                        <td>{{$item['produit']->designation}}</td>
                        <td>{{$item['produit']->prix_u}}</td>
                        <td>{{$item['qte']}}</td>
                        <td>{{$item['qte'] * $item['produit']->prix_u}} MAD</td>
                        <td>
                            <form action="{{route('home.delete',["id"=>$id])}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger" onclick="return confirm('Voulez-vous supprimer cette ligne?')">Supprimer</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <th colspan="4">Total</th>
                    <td colspan="2">{{$tot}} MAD</td>
                </tr>
            </tbody>
        </table>

        <a href="{{route('home.commander')}}" class="btn btn-primary">Commander</a>
        <a href="{{route('home.clear')}}" class="btn btn-warning">Vider le panier</a>
    </div>
@endif
@endsection
