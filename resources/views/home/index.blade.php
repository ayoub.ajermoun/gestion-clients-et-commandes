@extends('layouts.admin')
@section('title', 'Li')
@section('content')

    <div class="container">
        <h1>Liste des Produits</h1>
        <div class="container mx-auto mt-4">
        <div class="row catalogue">
            @foreach ($produits as $item)
                <div class="col-md-3 m-2" style="width: 18rem;">
                    <div class="card bg-light p-3">
                        <img src="{{ asset('storage/' . $item->photo) }}" class="card-img-top" alt="...">
                            
                        <div class="card-body">
                            <h5 class="card-title">{{$item->designation}}</h5>
                            @if($item->quantite_stock>0)
             <h5  class="badge badge-success bg-success">Availabele</h5>  
             @else
                 
             <h5 class="badge badge-success bg-danger">Unavailabele</h5>
              @endif
                            <p class="card-text">Prix :  <span class="badge badge-success bg-warning">{{$item->prix_u}}</span> MAD</p>
                            @if ($item->quantite_stock == 0)
                                <p class="text-danger">En rupture de stock</p>
                            @else
                                <p>En stock : {{$item->quantite_stock}}</p>
                                <form action="{{route('home.add',["id"=>$item->id])}}" method="POST">
                                    @csrf
                                    <label for="qte">Quantite</label>
                                    <input type="number" name="qte" id="qte"  size="5" min="1" max="{{$item->quantite_stock}}"> <br>
                                    <button type="submit" class="btn btn-primary mt-3">Acheter</button>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    </div>

@endsection
